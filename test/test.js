const DeMage = artifacts.require("DeMage");

/*
 * uncomment accounts to access the test accounts made available by the
 * Ethereum client
 * See docs: https://www.trufflesuite.com/docs/truffle/testing/writing-tests-in-javascript
 */
// require("chai").use(require("chai-as-promised")).should();

contract("DeMage", ([deployer, author, tipper]) => {
  let demage;
  let imgCount;
  before(async () => {
    demage = await DeMage.deployed();
  });

  describe("#User Interact", async () => {
    let result;
    it("Can upload Image", async () => {
      result = await demage.uploadImage("zxcvb1", "I have image", {
        from: author,
      });
      imgCount = await demage.imgCount();
    });
    
    it("Can tip the Author", async () => {
      await demage.tipAuthor(imgCount, { from: tipper });
    });
  });
});
