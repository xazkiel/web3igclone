//SPDX-License-Identifier: MIT
pragma solidity >=0.7.1;

contract DeMage {
    string public name = "DeMage";
    uint256 public imgCount = 0;

    mapping(uint256 => Image) public images;

    struct Image {
        uint256 id;
        string imHash;
        string desc;
        uint256 tipAmount;
        address payable author;
    }
    event ImageCreated(
        uint256 id,
        string imHash,
        string desc,
        uint256 tipAmount,
        address payable author
    );

    event ImageTipped(
        uint256 id,
        string imHash,
        string desc,
        uint256 tipAmount,
        address payable author
    );

    function uploadImage(string memory _imgHash, string memory _desc) public {
        require(bytes(_desc).length > 0);
        require(bytes(_imgHash).length > 0);
        require(msg.sender != address(0x0));

        images[imgCount] = Image(
            imgCount,
            _imgHash,
            _desc,
            0,
            payable(msg.sender)
        );

        emit ImageCreated(imgCount, _imgHash, _desc, 0, payable(msg.sender));
        imgCount++;
    }

    function tipAuthor(uint256 _id) public payable {
        Image memory _image = images[_id];
        address payable _author = _image.author;
        _author.transfer(msg.value);
        _image.tipAmount += msg.value;
        images[_id] = _image;

        emit ImageTipped(
            _image.id,
            _image.imHash,
            _image.desc,
            _image.tipAmount,
            _author
        );
    }
}
